package ritwik.compsci;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.awt.image.VolatileImage;
import java.io.IOException;
import java.net.URL;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.swing.JApplet;
import javax.swing.JColorChooser;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

;

/* This is the source code for the 'Poo in the Zoo'. 
 * Implemented by:
 * Howard Coffin
 * Ritwik Dutta
 * Ben Kim
 * */

public class pooZooAlternate extends JApplet implements KeyListener,
		MouseListener, MouseMotionListener, Runnable {
	// These two graphics objects are created for double-buffering purposes
	Graphics backGr, frontGr;
	// A list of names of the rooms, and a HUD name container list
	String[] roomNames, currentSurNames;
	// Image containers for all of the sprites in the game
	BufferedImage playerImg, animalImgs[], pooImg, buffImg, floorImgs[],
			hudImg, splashImg, deathImg, winImg;
	// Integer variables to store the majority of the game's data
	Integer pooNum, currentRoom, playerSpeed, animalSpeeds[], moveDirection[],
			currentLoopIter, maxLoopIter, directionCounterCurrent[],
			directionCounterMax[], playerHealth, playerLoc[], pooInRoom[];
	// Two dimensional arrays for convenient multiple-object data storage
	Integer[] pooLocs[], animalLocs[], doorZones[];
	// Boolean variables for various conditions in the game
	Boolean playerAlive, tigerFed, currentSplash, animalVisible[], firstRound;
	// URL objects for file paths of the various images
	URL playerURLs[], floorURLs[], hudURL, splashURL, pooURL, animalURLs[],
			deathURL, winURL;
	// A thread that runs the game; used for user-independent sprite motion
	Thread gameThread;
	// Random number generator for animal directional generators
	Random directGen;
	// Rectangle objects for collision detection
	Rectangle playerRect, animalRect, poopRects[];

	// Game initializer
	public void init() {
		resize(800, 600);

		// Adds all of the components the applet implements
		addKeyListener(this);
		addMouseListener(this);
		addMouseMotionListener(this);
		addMouseListener(this);
		addMouseMotionListener(this);
		setFocusable(true);

		// Initializes variables
		try {
			initVars();
		} catch (IOException e) {
		}
	}

	// Initializes all of the variables
	public void initVars() throws IOException {
		// Graphics system initializations
		buffImg = new BufferedImage(800, 600, BufferedImage.TYPE_INT_ARGB);
		backGr = buffImg.getGraphics();

		// Direction associated initializations
		directGen = new Random();
		moveDirection = new Integer[4];
		directionCounterCurrent = new Integer[] { 0, 0, 0, 0 };
		directionCounterMax = new Integer[4];

		// Main loop index controllers
		currentLoopIter = 0;
		maxLoopIter = 250;

		// Animal related initializations
		animalSpeeds = new Integer[] { 2, 2, 15, 20 };
		animalLocs = new Integer[4][2];
		animalVisible = new Boolean[] { true, true, true, true };
		animalURLs = new URL[4];
		animalImgs = new BufferedImage[4];
		animalRect = new Rectangle(400, 300, 50, 50);
		animalURLs[0] = this.getClass().getResource(
				"pooZooAssets/elephantIcon.png");
		animalURLs[1] = this.getClass().getResource(
				"pooZooAssets/giraffeIcon.png");
		animalURLs[2] = this.getClass().getResource(
				"pooZooAssets/tigerIcon.png");
		animalURLs[3] = this.getClass().getResource(
				"pooZooAssets/snakeIcon.png");
		animalImgs[0] = ImageIO.read(animalURLs[0]);
		animalImgs[1] = ImageIO.read(animalURLs[1]);
		animalImgs[2] = ImageIO.read(animalURLs[2]);
		animalImgs[3] = ImageIO.read(animalURLs[3]);
		for (int i = 0; i < 4; i++) {
			animalLocs[i][0] = 400;
			animalLocs[i][1] = 300;
		}

		// Player related initializations
		playerSpeed = 15;
		playerHealth = 30;
		playerRect = new Rectangle(400, 300, 50, 50);
		playerLoc = new Integer[2];
		playerLoc[0] = 400;
		playerLoc[1] = 300;
		playerURLs = new URL[3];
		playerURLs[0] = this.getClass().getResource(
				"pooZooAssets/playerGoodIcon.png");
		playerURLs[1] = this.getClass().getResource(
				"pooZooAssets/playerMidIcon.png");
		playerURLs[2] = this.getClass().getResource(
				"pooZooAssets/playerSadIcon.png");
		playerImg = ImageIO.read(playerURLs[0]);

		// Index of current room
		currentRoom = 0;

		// Boolean state variables and related initializations
		tigerFed = false;
		playerAlive = true;
		currentSplash = true;
		splashURL = this.getClass()
				.getResource("pooZooAssets/splashScreen.png");
		splashImg = ImageIO.read(splashURL);
		deathURL = this.getClass().getResource("pooZooAssets/deathImage.png");
		deathImg = ImageIO.read(deathURL);
		winURL = this.getClass().getResource("pooZooAssets/winImage.png");
		winImg = ImageIO.read(winURL);

		// Poop related initializations
		pooInRoom = new Integer[6];
		pooNum = 0;
		firstRound = false;
		pooLocs = new Integer[6][2];
		poopRects = new Rectangle[6];
		pooURL = this.getClass().getResource("pooZooAssets/poopIcon.png");
		pooImg = ImageIO.read(pooURL);
		for (int i = 0; i < 6; i++) { // Randomly place poop objects
			pooLocs[i][0] = directGen.nextInt(600) + 51;
			pooLocs[i][1] = directGen.nextInt(400) + 51;
			poopRects[i] = new Rectangle(pooLocs[i][0], pooLocs[i][1], 50, 50);
			pooInRoom[i] = 0;
		}

		// Room related initializations
		roomNames = new String[6];
		roomNames[0] = "Lobby";
		roomNames[1] = "Elephant";
		roomNames[2] = "Giraffe";
		roomNames[3] = "Tiger";
		roomNames[4] = "Snake";
		roomNames[5] = "Trash";

		// Door zone initializations
		// These define where doors exist, and if they do, to what room
		doorZones = new Integer[6][4];
		doorZones[0][0] = 6;
		doorZones[0][1] = 6;
		doorZones[0][2] = 1;
		doorZones[0][3] = 3;
		doorZones[1][0] = 0;
		doorZones[1][1] = 6;
		doorZones[1][2] = 2;
		doorZones[1][3] = 4;
		doorZones[2][0] = 1;
		doorZones[2][1] = 6;
		doorZones[2][2] = 6;
		doorZones[2][3] = 5;
		doorZones[3][0] = 6;
		doorZones[3][1] = 0;
		doorZones[3][2] = 4;
		doorZones[3][3] = 6;
		doorZones[4][0] = 3;
		doorZones[4][1] = 1;
		doorZones[4][2] = 5;
		doorZones[4][3] = 6;
		doorZones[5][0] = 4;
		doorZones[5][1] = 2;
		doorZones[5][2] = 6;
		doorZones[5][3] = 6;

		// HUD related initializations
		backGr.setFont(new Font("Arial", Font.PLAIN, 20));
		backGr.setColor(Color.red);
		currentSurNames = new String[4];
		currentSurNames[0] = "";
		currentSurNames[1] = "";
		currentSurNames[2] = "";
		currentSurNames[3] = "";
		hudURL = this.getClass().getResource("pooZooAssets/hudImage.png");
		hudImg = ImageIO.read(hudURL);

		// Background graphic initializations
		floorURLs = new URL[6];
		floorImgs = new BufferedImage[6];
		floorURLs[0] = this.getClass().getResource(
				"pooZooAssets/lobbyFloor.jpg");
		floorImgs[0] = ImageIO.read(floorURLs[0]);
		floorURLs[1] = this.getClass().getResource(
				"pooZooAssets/elephantFloor.jpg");
		floorImgs[1] = ImageIO.read(floorURLs[1]);
		floorURLs[2] = this.getClass().getResource(
				"pooZooAssets/giraffeFloor.jpg");
		floorImgs[2] = ImageIO.read(floorURLs[2]);
		floorURLs[3] = this.getClass().getResource(
				"pooZooAssets/tigerFloor.jpg");
		floorImgs[3] = ImageIO.read(floorURLs[3]);
		floorURLs[4] = this.getClass().getResource(
				"pooZooAssets/snakeFloor.jpg");
		floorImgs[4] = ImageIO.read(floorURLs[4]);
		floorURLs[5] = this.getClass().getResource(
				"pooZooAssets/trashFloor.jpg");
		floorImgs[5] = ImageIO.read(floorURLs[5]);
	}

	// Start game thread
	public void start() {
		if (gameThread == null) {
			gameThread = new Thread(this, "pooZooMain");
			gameThread.start();
		}
	}

	// Runs all main loop functions
	public void run() {
		// Gets current thread and checks for match with game thread
		Thread currentlyRunning = Thread.currentThread();
		while (gameThread == currentlyRunning) {

			// Checks if conditions are right and draws HUD if so
			if (currentSplash == false) {
				if (tigerFed == false) {
					if (playerAlive) {
						drawHud();
					}
				}
				/*
				 * This bit of code takes some explanation. It makes sure the
				 * animals only move every couple hundred cycles or so, as
				 * otherwise they move way too fast. The checking system only
				 * runs the update function if the iteration is 0, and otherwise
				 * it increments the iterator until it reaches the max value, at
				 * which point the current iterator is reset to 0.
				 */
				if (currentLoopIter == 0) {
					updateAnimalLocs();
					currentLoopIter++;
				} else {
					if (currentLoopIter < maxLoopIter) {
						currentLoopIter++;
					} else {
						currentLoopIter = 0;
					}
				}
				// Repaints everything on screen so the animals move
				repaint();
			}
		}
	}

	// Default update function
	public void update(Graphics grObj) {
		paint(grObj);
	}

	// Paint all the data to the canvas
	public void paint(Graphics frontGr) {
		// Resize the window to te set resolution
		resize(800, 600);

		// Logic flow to determine the proper course of action
		if (currentSplash == false) {
			if (tigerFed == false) {
				if (playerAlive == true) {

					// Draw background image
					backGr.drawImage(floorImgs[currentRoom], 0, 0, this);
					backGr.drawImage(playerImg, playerLoc[0], playerLoc[1],
							this);

					// Draw HUD Image
					backGr.drawImage(hudImg, 0, 0, this);

					// Determine if animal is present and visible
					if (currentRoom > 0 & currentRoom < 5) {
						if (animalVisible[currentRoom - 1] == true) {
							// Draw animal
							backGr.drawImage(animalImgs[currentRoom - 1],
									animalLocs[currentRoom - 1][0],
									animalLocs[currentRoom - 1][1], this);
						}
					}

					// Determine if poo is present and visible
					if (pooInRoom[currentRoom] == 0
							|| pooInRoom[currentRoom] == 1) {
						// Draw poo
						backGr.drawImage(pooImg, pooLocs[currentRoom][0],
								pooLocs[currentRoom][1], this);
					}
				} else { // Death condition, draw death image
					backGr.drawImage(deathImg, 0, 0, this);
				}
			} else { // Win condition, draw win image
				backGr.drawImage(winImg, 0, 0, this);
			}
		} else { // First start, draw splash screen image
			backGr.drawImage(splashImg, 0, 0, this);
		}

		// Draw the buffered image to the main graphics object
		frontGr.drawImage(buffImg, 0, 0, this);
	}

	// Key press and controls handler
	public void keyPressed(KeyEvent e) {

		// WSAD control block
		if (e.getKeyCode() == 87) {
			playerLoc[1] -= playerSpeed;
		} else if (e.getKeyCode() == 83) {
			playerLoc[1] += playerSpeed;
		} else if (e.getKeyCode() == 65) {
			playerLoc[0] -= playerSpeed;
		} else if (e.getKeyCode() == 68) {
			playerLoc[0] += playerSpeed;
		}

		// Arrow keys control block
		else if (e.getKeyCode() == 38) {
			playerLoc[1] -= playerSpeed;
		} else if (e.getKeyCode() == 40) {
			playerLoc[1] += playerSpeed;
		} else if (e.getKeyCode() == 37) {
			playerLoc[0] -= playerSpeed;
		} else if (e.getKeyCode() == 39) {
			playerLoc[0] += playerSpeed;
		}

		// Check if player is out of boundaries and fix problems
		if (playerLoc[0] < 0) {
			playerLoc[0] = 0;
		} else if (playerLoc[0] > 750) {
			playerLoc[0] = 750;
		}
		if (playerLoc[1] < 50) {
			playerLoc[1] = 50;
		} else if (playerLoc[1] > 500) {
			playerLoc[1] = 500;
		}

		// Check if player has walked into a door zone
		doorCheck();

		// Update player rectangle for collision detection
		playerRect.x = playerLoc[0];
		playerRect.y = playerLoc[1];

		// If poo is in room, check if player has touched it
		if (pooInRoom[currentRoom] == 0 || pooInRoom[currentRoom] == 1) {
			if (playerRect.intersects(poopRects[currentRoom])) {
				pooInRoom[currentRoom] = 2;
				pooNum++;
			}
		}

		if (pooNum == 6 & firstRound == false) {
			firstRound = true;
			for (int i = 0; i < 4; i++) {
				System.out.println(roomNames[i + 1]);
				pooLocs[i + 1][0] = animalLocs[i][0];
				pooLocs[i + 1][1] = animalLocs[i][1];
				if (pooLocs[i + 1][0] < 50) {
					pooLocs[i + 1][0] = 50;
				} else if (pooLocs[i + 1][0] > 700) {
					pooLocs[i + 1][0] = 50;
				}
				if (pooLocs[i + 1][1] < 100) {
					pooLocs[i + 1][1] = 100;
				} else if (pooLocs[i + 1][1] > 500) {
					pooLocs[i + 1][1] = 500;
				}
				poopRects[i + 1] = new Rectangle(pooLocs[i + 1][0],
						pooLocs[i + 1][1], 50, 50);
				pooInRoom[i + 1] = 1;
			}
		}
	}

	// Draw the HYD
	public void drawHud() {

		// Set up the labels for the surrounding rooms
		for (int i = 0; i < 4; i++) {
			if (doorZones[currentRoom][i] < 6) {
				currentSurNames[i] = roomNames[doorZones[currentRoom][i]];
			} else {
				currentSurNames[i] = "No door";
			}
		}

		// Draw all of the HUD lables
		backGr.drawString("Current: " + roomNames[currentRoom], 25, 30);
		backGr.drawString("Left: " + currentSurNames[0], 200, 30);
		backGr.drawString("Right: " + currentSurNames[2], 350, 30);
		backGr.drawString("Top: " + currentSurNames[1], 500, 30);
		backGr.drawString("Down: " + currentSurNames[3], 650, 30);
		backGr.drawString("Poop Collected: " + pooNum, 25, 580);
		backGr.drawString("Player Health: " + playerHealth, 200, 580);
		if ((10 - pooNum) > 1) {
			backGr.drawString("Collect " + (10 - pooNum)
					+ " more pieces of poo.", 375, 580);
		} else if ((10 - pooNum) == 1) {
			backGr.drawString("Collect " + (10 - pooNum)
					+ " more piece of poo.", 375, 580);
		} else if ((10 - pooNum) == 0) {
			backGr.drawString("Go feed the tiger now.", 375, 580);
		}
	}

	// Check if a player has touched a door
	public void doorCheck() {

		// Right door
		if (playerLoc[0] > 700) {
			if (doorZones[currentRoom][2] < 6) {
				playerLoc[0] = 50;
				currentRoom = doorZones[currentRoom][2];
			}

		}

		// Left door
		else if (playerLoc[0] < 50)
			if (doorZones[currentRoom][0] < 6) {
				{
					playerLoc[0] = 700;
					currentRoom = doorZones[currentRoom][0];
				}
			}

		// Bottom door
		if (playerLoc[1] > 450) {
			if (doorZones[currentRoom][3] < 6) {
				playerLoc[1] = 100;
				currentRoom = doorZones[currentRoom][3];
			}
		}

		// Top door
		if (playerLoc[1] < 100) {
			if (doorZones[currentRoom][1] < 6) {
				playerLoc[1] = 450;
				currentRoom = doorZones[currentRoom][1];
			}
		}

	}

	// Moves animals independent of what player presses
	public void updateAnimalLocs() {
		// Iterate through list of animals
		for (int i = 0; i < 4; i++) {
			/*
			 * Another moment that needs some explanation. I've implemented
			 * another cycle-based counter to make sure that the animals move in
			 * more sensible ways by continuing for at least 100 cycles in
			 * whatever direction, and turning around at walls
			 */
			if (directionCounterCurrent[i] == 0) {
				// Randomly generate new direction as well as length of next
				// movement.
				moveDirection[i] = directGen.nextInt(4);
				directionCounterMax[i] = directGen.nextInt(100) + 100;
				directionCounterCurrent[i]++;
			} else {
				if (directionCounterCurrent[i] > directionCounterMax[i]) {
					directionCounterCurrent[i] = 0;
				} else {
					directionCounterCurrent[i]++;
				}
			}

			// Actually update the animal location
			if (moveDirection[i] == 0) {
				animalLocs[i][0] -= animalSpeeds[i];
			} else if (moveDirection[i] == 1) {
				animalLocs[i][1] -= animalSpeeds[i];
			} else if (moveDirection[i] == 2) {
				animalLocs[i][0] += animalSpeeds[i];
			} else if (moveDirection[i] == 3) {
				animalLocs[i][1] += animalSpeeds[i];
			}

			// Fix any problems with crossing the boundaries
			if (animalLocs[i][1] > 500) {
				animalLocs[i][1] = 500;
				moveDirection[i] = 1;
			} else if (animalLocs[i][1] < 50) {
				animalLocs[i][1] = 50;
				moveDirection[i] = 3;
			}
			if (animalLocs[i][0] > 750) {
				animalLocs[i][0] = 750;
				moveDirection[i] = 0;
			} else if (animalLocs[i][0] < 50) {
				animalLocs[i][0] = 50;
				moveDirection[i] = 2;
			}

			// Check if player is in the current room
			if (currentRoom == (i + 1)) {

				// Check for collisions to determine win/lose/death condition
				if (playerRect.intersects(new Rectangle(animalLocs[i][0],
						animalLocs[i][1], 50, 50))) {

					// Check if player has all the necessary poo
					if (pooNum < 10) {

						// Decrement health
						playerHealth--;

						// Change sprites based upon damage level
						if (playerHealth < 20) {
							try {
								// Medium face
								playerImg = ImageIO.read(playerURLs[1]);
							} catch (IOException e) {
							}
						}
						if (playerHealth < 10) {
							try { // Sad face
								playerImg = ImageIO.read(playerURLs[2]);
							} catch (IOException e) {
							}
						}
						if (playerHealth == 0) {
							// Flip losing condition
							playerAlive = false;
						}
					} else {
						// If so , check if player is in tiger room
						if (currentRoom != 3) {
							// If not, make the animal in the room invisible
							animalVisible[currentRoom - 1] = false;
						} else {
							// If so, set winning condition
							tigerFed = true;
						}
					}
				}
			}
		}
	}

	// Auto-generated dummy function
	public void keyReleased(KeyEvent e) {
	}

	// Auto-generated dummy function
	public void keyTyped(KeyEvent e) {
	}

	// Mouse clicked function handler
	public void mouseClicked(MouseEvent e) {
		// If currently displaying splash screen, stop
		if (currentSplash == true) {
			currentSplash = false;

			// If the player has won or lost
		} else if (tigerFed == true || playerAlive == false) {
			// If so, reset player position and variables
			playerLoc[0] = 400;
			playerLoc[1] = 300;
			currentRoom = 0;
			playerAlive = true;
			tigerFed = false;

			// Randomly generate new positions for the poo
			for (int i = 0; i < 6; i++) { // Randomly place poop objects
				pooLocs[i][0] = directGen.nextInt(600) + 51;
				pooLocs[i][1] = directGen.nextInt(400) + 51;
				poopRects[i] = new Rectangle(pooLocs[i][0], pooLocs[i][1], 50,
						50);
				pooInRoom[i] = 0;
			}
			playerHealth = 30;
			pooNum = 0;
			firstRound = false;

			// Reset the player sprite
			try {
				playerImg = ImageIO.read(playerURLs[0]);
			} catch (IOException e1) {
			}
		}
	}

	// Auto-generated dummy function
	public void mousePressed(MouseEvent e) {
	}

	// Auto-generated dummy function
	public void mouseReleased(MouseEvent e) {
	}

	// Auto-generated dummy function
	public void mouseEntered(MouseEvent e) {
	}

	// Auto-generated dummy function
	public void mouseExited(MouseEvent e) {
	}

	// Auto-generated dummy function
	public void mouseDragged(MouseEvent e) {
	}

	// Auto-generated dummy function
	public void mouseMoved(MouseEvent e) {
	}

}